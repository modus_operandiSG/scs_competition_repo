﻿using UnityEngine;
using System.Collections;

public class Interface_Control : MonoBehaviour {

	public Vector2 startTouch = Vector2.zero;


	// Use this for initialization
	void Start () 
	{
		GameObject startButton = GameObject.Find ("StartButton");

		startButton.renderer.enabled = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		foreach (Touch touch in Input.touches) 
		{
			startTouch = touch.position;
			Ray ray = Camera.main.ScreenPointToRay(touch.position);
			RaycastHit hit;

			if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began) 
			{
				if(Physics.Raycast(ray, out hit))
				{
					GameObject sound = GameObject.Find ("AudioManager");
					SoundManager startSound = sound.GetComponent<SoundManager>();

					 //Quit Button Interface
					GameObject QuitButton = GameObject.Find ("QuitButton");
					if(hit.collider.gameObject == QuitButton)
					{
						////Quits the Application
						Application.Quit();
					}
					
					// Start Button Interface
					GameObject startButton = GameObject.Find ("StartButton");
					if(hit.collider.gameObject == startButton)
					{
						startSound.StartButton();

						startButton.renderer.enabled = true;
						//Loads Main menu.
						FadeTransition.LoadLevel("MainMenuScene", 0.5f,0.5f,Color.black);
					}
					else
					{
						renderer.enabled = false;
					}
				}
			}
		}
	}
}
