﻿using UnityEngine;
using System.Collections;

public class InterfaceControl : MonoBehaviour 
{

	public Vector2 startTouch = Vector2.zero;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		foreach (Touch touch in Input.touches) 
		{
			startTouch = touch.position;
			Ray ray = Camera.main.ScreenPointToRay(touch.position);
			RaycastHit hit;
			
			if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began) 
			{
				if(Physics.Raycast(ray, out hit))
				{
					//GameObject sound = GameObject.Find ("AudioManager");
					//SoundManager startSound = sound.GetComponent<SoundManager>();
					
					//BackButton Interface
					GameObject BackButton = GameObject.Find ("BackButton");
					GameObject sound = GameObject.Find ("AudioManager");
					SoundManager backSound = sound.GetComponent<SoundManager>();
					if(hit.collider.gameObject == BackButton)
					{
						backSound.MenuButtons();
						//Loads Main menu.
						FadeTransition.LoadLevel("MainMenuScene", 0.5f,0.5f,Color.black);
					}
				}
			}
		}
	}
}
