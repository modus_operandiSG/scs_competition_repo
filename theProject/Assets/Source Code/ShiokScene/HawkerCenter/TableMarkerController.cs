﻿using UnityEngine;
using System.Collections;

public enum CORNER
{
	BOTTOM_LEFT,
	TOP_RIGHT,
	BOTTOM_RIGHT,
	TOP_LEFT,
};

public enum COLOR
{
	GREEN,
	PURPLE,
	RED,
	BLUE,
	YELLOW,
};

public class TableMarkerController : MonoBehaviour 
{
	private Color[] AvailableColors;
	private ArrayList GroupIDs;

	private Color ConvertRGBColor(int Red,int Green,int Blue)
	{
		return ( new Color((float)(Red/255.0f),(float)(Green/255.0f),(float)(Blue/255.0f)) );
	}

	private void Show()
	{
		gameObject.GetComponent<MeshRenderer> ().enabled = true;
	}

	private void Hide()
	{
		gameObject.GetComponent<MeshRenderer> ().enabled = false;
	}

	public void AddColor(int groupID)
	{
		if ( !GroupIDs.Contains(groupID) )
		{
			Mesh mesh = gameObject.GetComponent<MeshFilter>().mesh;
			Color[] colors;
			if ( GroupIDs.Count == 0 )
			{
				colors = new Color[4];
				Show();
				colors[(int)CORNER.TOP_LEFT] = AvailableColors[groupID];
				colors[(int)CORNER.BOTTOM_LEFT] = AvailableColors[groupID];
				colors[(int)CORNER.TOP_RIGHT] = AvailableColors[groupID];
				colors[(int)CORNER.BOTTOM_RIGHT] = AvailableColors[groupID];
			}
			else if ( GroupIDs.Count == 1 )
			{
				colors = mesh.colors;
				colors[(int)CORNER.TOP_RIGHT] = AvailableColors[groupID];
				colors[(int)CORNER.BOTTOM_RIGHT] = AvailableColors[groupID];
			}
			else if ( GroupIDs.Count == 2 )
			{
				colors = mesh.colors;
				colors[(int)CORNER.TOP_LEFT] = AvailableColors[groupID];
			}
			else if ( GroupIDs.Count == 3 )
			{
				colors = mesh.colors;
				colors[(int)CORNER.TOP_RIGHT] = AvailableColors[groupID];
			}
			else
			{
				colors = mesh.colors;
			}
			mesh.colors = colors;
		}
		GroupIDs.Add (groupID);
	}

	public void RemoveColor(int groupID)
	{
		if ( GroupIDs.Contains(groupID) )
		{
			GroupIDs.Remove(groupID);
			Mesh mesh = gameObject.GetComponent<MeshFilter>().mesh;
			Color[] colors = mesh.colors;
			if ( GroupIDs.Count == 3 )
			{
				colors[(int)CORNER.TOP_RIGHT] = colors[(int)CORNER.BOTTOM_RIGHT];
			}
			else if ( GroupIDs.Count == 2 )
			{
				colors[(int)CORNER.TOP_LEFT] = colors[(int)CORNER.BOTTOM_LEFT];
			}
			else if ( GroupIDs.Count == 1 )
			{
				colors[(int)CORNER.TOP_RIGHT] = colors[(int)CORNER.BOTTOM_LEFT];
				colors[(int)CORNER.BOTTOM_RIGHT] = colors[(int)CORNER.BOTTOM_LEFT];
			}
			else if ( GroupIDs.Count == 0 )
			{
				Hide();
			}
			mesh.colors = colors;
		}
	}

	// Use this for initialization
	void Start () 
	{
		AvailableColors = new Color[5];
		AvailableColors[(int)COLOR.GREEN] = ConvertRGBColor (59,211,66);
		AvailableColors[(int)COLOR.PURPLE] = ConvertRGBColor (195,88,214);
		AvailableColors[(int)COLOR.RED] = ConvertRGBColor (235,50,84);
		AvailableColors[(int)COLOR.BLUE] = ConvertRGBColor (88,151,215);
		AvailableColors[(int)COLOR.YELLOW] = ConvertRGBColor (235,50,84);
		GroupIDs = new ArrayList();
		Hide();
	}
	
	// Update is called once per frame
	void Update () 
	{
	}
}
