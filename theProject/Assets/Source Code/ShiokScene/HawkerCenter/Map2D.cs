﻿using UnityEngine;
using System.Collections;


public class Grid
{
	public int x;
	public int y;
	public float posX;
	public float posY;
	public int F;
	public int G;
	public int H;
	public float size;
	public float oblique{ get; private set; }
	public Grid parentGrid;
	public bool IsReachable;

	public Grid (float Size, int X, int Y, float PosX, float PosY)
	{
		IsReachable = true;
		size = Size;
		oblique = (float)System.Math.Sqrt(size * size + size * size);
		x = X;
		y = Y;
		posX = PosX;
		posY = PosY;
	}

	public void CalcF()
	{
		F = G + H;
	}
}	

public class Map2D
{
	public ArrayList gridArrayList;
	private ArrayList openList;
	private ArrayList closeList;
	public int[,] mazeArray { get; private set; }

	public int numOfRow;
	public int numOfCol;
	public float mapWidth;
	public float mapHeight;
	public float gridSize { get; private set; }

	public Grid GetGrid(int X, int Y)
	{
		return (Grid)gridArrayList[Y*numOfCol+X];
	}

	public void MarkAZoneUnreachable(float TopLeftPointX, float TopLeftPointY, float BottomRightPointX, float BottomRightPointY)
	{
		TopLeftPointX += mapWidth / 2;
		BottomRightPointX += mapWidth / 2;
		TopLeftPointY += mapHeight / 2;
		BottomRightPointY += mapHeight / 2;

//		int temp_numOfRow = (int)((BottomRightPointY - TopLeftPointY)/gridSize)+1;
//		int temp_numOfCol = (int)((BottomRightPointX - TopLeftPointX)/gridSize)+1;
//		Debug.Log(temp_numOfRow);
//		Debug.Log(temp_numOfCol);
//		Debug.Log (TopLeftPointX);
//		Debug.Log (BottomRightPointX);
//		Debug.Log (TopLeftPointY);
//		Debug.Log (BottomRightPointY);
		for ( int i = (int)(TopLeftPointY/gridSize) ; i < (int)(BottomRightPointY/gridSize) ; i++ )
		{
			for ( int j = (int)(TopLeftPointX/gridSize) ; j < (int)(BottomRightPointX/gridSize) ; j++ )
			{
				GetGrid(j,i).IsReachable = false;
				//Print(GetGrid(j,i));
			}
		}
	}

	private float CalcGridPosX(int X)
	{
		return (X-numOfCol/2)*gridSize + gridSize / 2;
	}

	private float CalcGridPosY(int Y)
	{
		return (Y-numOfRow/2)*gridSize + gridSize / 2;
	}

	public void Init(float MapWidth, float MapHeight, float GridSize)
	{
		numOfRow = (int)System.Math.Ceiling(MapHeight/GridSize);
		numOfCol = (int)System.Math.Ceiling(MapWidth/GridSize);
		mapWidth = MapWidth;
		mapHeight = MapHeight;
		gridSize = GridSize;
		
		gridArrayList = new ArrayList ();
		for ( int i = 0 ; i < numOfRow ; i++ )
		{
			for ( int j = 0 ; j < numOfCol ; j++ )
			{
				Grid newGrid = new Grid(gridSize,j,i,CalcGridPosX(j),CalcGridPosY(i));
				gridArrayList.Add(newGrid);
			}
		}
		
		foreach ( GameObject obj in GameObject.FindGameObjectsWithTag("Obj"))
		{
			float TopLeftPointX = (obj.transform.position.x+obj.transform.collider.bounds.center.x-
				obj.transform.collider.bounds.size.x)/2;
			float TopLeftPointY = (obj.transform.position.y+obj.transform.collider.bounds.center.y-
				obj.transform.collider.bounds.size.y)/2;
			float BottomRightPointX = (obj.transform.position.x+obj.transform.collider.bounds.center.x+
				obj.transform.collider.bounds.size.x)/2;
			float BottomRightPointY = (obj.transform.position.y+obj.transform.collider.bounds.center.y+
				obj.transform.collider.bounds.size.y)/2;
			
			MarkAZoneUnreachable(TopLeftPointX, TopLeftPointY, BottomRightPointX, BottomRightPointY);
//			Debug.Log(obj.name);
//			Debug.Log ("X"+TopLeftPointX.ToString());
//			Debug.Log ("Y"+TopLeftPointY.ToString());
//			Debug.Log ("X"+BottomRightPointX.ToString());
//			Debug.Log ("Y"+BottomRightPointY.ToString());
		}
	}

//	public Map2D(float MapWidth, float MapHeight, float GridSize)
//	{
//
//	}

	private bool CanReach(Grid StartGrid ,Grid CurrentGrid, bool IsIgnoreCorner)
	{
//		Debug.Log ("s");
//		Print (StartGrid);
//		Debug.Log ("c");
//		Print (CurrentGrid);

		if ( GetGrid(CurrentGrid.x,CurrentGrid.y).IsReachable == false || IsGridExistInList(closeList,CurrentGrid) )
		{
			//Debug.Log ("1");
			return false;
		}
		else
		{
			if (System.Math.Abs(CurrentGrid.x - StartGrid.x) + System.Math.Abs(CurrentGrid.y - StartGrid.y) == 1)
			{
				//Debug.Log ("2");
				return true;
			}
			else
			{
				if ( GetGrid(System.Math.Abs(CurrentGrid.x - 1), CurrentGrid.y).IsReachable == true && 
				    GetGrid(CurrentGrid.x, System.Math.Abs(CurrentGrid.y - 1)).IsReachable == true )
				{
					//Debug.Log ("2");
					return true;
				}
				else
				{
					//Debug.Log ("3");
					return IsIgnoreCorner;
				}
			}
		}
	}

	private void Print(Grid grid)
	{
		Debug.Log (grid.x + " ," + grid.y);
	}

	private ArrayList GetSurroundGrids (Grid grid)
	{
		ArrayList surroundGrids = new ArrayList();
		for ( int i = ( grid.x-1 > 0 ? grid.x-1 : 0 ); 
		     i <= ( grid.x+1 >= numOfCol ? grid.x : grid.x+1 );
		     i++ )
		{
			for ( int j = ( grid.y-1 > 0 ? grid.y-1 : 0 );
			     j <= ( grid.y+1 >= numOfRow ? grid.y : grid.y+1 );
			     j++ )
			{
				Grid temp_grid = new Grid(gridSize,i,j,CalcGridPosX(i),CalcGridPosY(j));
				if ( CanReach(grid,temp_grid,true) )
				{
					surroundGrids.Add(temp_grid);
				}
			}
		}
		return surroundGrids;
	}

	private Grid GetMinFGrid(ArrayList List)
	{
		int F = 9999999;
		Grid result = null;
		foreach ( Grid grid in List )
		{
			if ( grid.F < F )
			{
				result = grid;
				F = grid.F;
			}
		}
		return result;
	}

	private int CalcG(Grid StartGrid, Grid CurrentGrid)
	{
		int G = (System.Math.Abs(CurrentGrid.x - StartGrid.x) + System.Math.Abs(CurrentGrid.y - StartGrid.y)) == 2 ? (int)CurrentGrid.size : (int)CurrentGrid.oblique;
		int parentG = CurrentGrid.parentGrid != null ? CurrentGrid.parentGrid.G : 0;
		return G + parentG;
	}

	private int CalcH(Grid EndGrid, Grid CurrentGrid)
	{
		int step = System.Math.Abs(CurrentGrid.x - EndGrid.x) + System.Math.Abs(CurrentGrid.y - EndGrid.y);
		return step * (int)CurrentGrid.size;
	}

	private Grid GetGridFromList(ArrayList List, int X, int Y)
	{
		foreach ( Grid grid in List )
		{
			if ( grid.x == X && grid.y == Y )
				return grid;
		}
		return null;
	}

	private bool IsGridExistInList(ArrayList List, Grid theGrid)
	{
		foreach( Grid grid in List )
		{
			if ( theGrid.x == grid.x && theGrid.y == grid.y )
				return true;
		}
		return false;
	}

	private ArrayList GeneratPath( Grid grid )
	{
		ArrayList path = new ArrayList();
		while( grid != null )
		{
			//Debug.Log(grid.x + ", " + grid.y);
			if ( grid != null )
				path.Add(grid);
			grid = grid.parentGrid;
		}	
		return path;
	}

	public ArrayList FindPath(int StartX, int StartY, int EndX, int EndY)
	{
		openList = new ArrayList ();
		closeList = new ArrayList ();
//		Debug.Log(StartX);
//		Debug.Log(StartY);
//		Debug.Log(EndX);
//		Debug.Log(EndY);
		if ( StartX >= 0 && StartY >= 0 && EndX < numOfCol && EndY < numOfRow )
		{

			Grid StartGrid = new Grid (gridSize,StartX,StartY,CalcGridPosX(StartX),CalcGridPosY(StartY));
			Grid EndGrid = new Grid (gridSize,EndX,EndY,CalcGridPosX(EndX),CalcGridPosY(EndY));
			openList.Add (StartGrid);
			while(openList.Count != 0)
			{
				Grid TempStartGrid = GetMinFGrid(openList);
				openList.RemoveAt(0);
				closeList.Add(TempStartGrid);
				
				ArrayList SurroundGrids = GetSurroundGrids(TempStartGrid);
				
				foreach ( Grid grid in SurroundGrids )
				{
					if ( IsGridExistInList(openList,grid) )
					{
						int G = CalcG(StartGrid,grid);
						if ( G < grid.G )
						{
							grid.parentGrid = TempStartGrid;
							grid.G = G;
							grid.CalcF();
						}
					}
					else
					{
						grid.parentGrid = TempStartGrid;
						grid.G = CalcG(TempStartGrid,grid);
						grid.H = CalcH(EndGrid,grid);
						grid.CalcF();
						openList.Add(grid);
					}
				}
				if ( GetGridFromList(openList,EndX,EndY) != null )
				{
					return GeneratPath(GetGridFromList(openList,EndX,EndY));
				}
			}
		}
		return null;
	}
}
