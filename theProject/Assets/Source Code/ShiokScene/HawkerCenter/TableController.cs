﻿using UnityEngine;
using System.Collections;

public enum SEAT_STATE
{
	EMPTY,
	CHOPE,
	TAKEN,
};

public class TableController : MonoBehaviour 
{
	public int MAX_SEAT = 4;
	public SEAT_STATE[] seatState;
	public GameObject ChopePrefab;
	private GameObject ChopeObj;
	public GameObject TableMarker;
	public GameObject EatingSpritePrefab;
	private GameObject EatingSpriteObj;
	public int NumOfCustomerAreEating;
	public bool IsTakenByDurainUncle;

	// Use this for initialization
	void Start () 
	{
		NumOfCustomerAreEating = 0;
		seatState = new SEAT_STATE[MAX_SEAT];
		for ( int i= 0 ; i < MAX_SEAT ; i++ )
		{
			seatState[i] = SEAT_STATE.EMPTY;
		}
		EatingSpriteObj = null;
		IsTakenByDurainUncle = false;
		ChopeObj = null;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if ( NumOfCustomerAreEating > 0 )
		{
			if ( EatingSpriteObj == null )
			{
				CreateAndShowEatingSprite();
			}
		}
		else
		{
			DestroyEatingSprite();
		}
	}

	public void CreateAndShowEatingSprite()
	{
		EatingSpriteObj = (GameObject)Instantiate(EatingSpritePrefab,
		                                          gameObject.transform.position,
		                                          gameObject.transform.rotation);
	}

	public void CreateAndShowChopeObj()
	{
		DestroyChopeObj ();
		ChopeObj = (GameObject)Instantiate(ChopePrefab,
		                                   gameObject.transform.position + new Vector3(0,0,-1.0f),
		                                   gameObject.transform.rotation);
	}

	public void DestroyChopeObj()
	{
		if ( ChopeObj != null )
			Destroy (ChopeObj);
		ChopeObj = null;
	}
	
	public Vector3 GetSitDownPoint()
	{
		return gameObject.transform.position + new Vector3 (0, 0.5f, 0);
	}

	public Vector3 GetStandUpPoint()
	{
		return gameObject.transform.position + new Vector3 (0, 3.0f, 0);
	}

	public void LeaveSeat(int groupID,int SeatIndex)
	{
		seatState [SeatIndex] = SEAT_STATE.EMPTY;
		TableMarker.GetComponent<TableMarkerController> ().RemoveColor (groupID);
	}

	public void ChopeSeat(int SeatIndex)
	{
		seatState [SeatIndex] = SEAT_STATE.CHOPE;
	}
	
	public void TakeSeat(int groupID,int SeatIndex)
	{
		seatState [SeatIndex] = SEAT_STATE.TAKEN;
		TableMarker.GetComponent<TableMarkerController> ().AddColor (groupID);
	}
	
	public Vector3 GetSeatPosition(int SeatIndex)
	{
		if ( gameObject.name.Contains("1") )
		{
			if ( SeatIndex < MAX_SEAT/2 )
				return transform.position + new Vector3 (-1.7f-0.3f*(SeatIndex%(MAX_SEAT/2)), -SeatIndex%(MAX_SEAT/2)+1.5f, 0);
			else
				return transform.position + new Vector3 (1.7f-0.3f*(SeatIndex%(MAX_SEAT/2)), -SeatIndex%(MAX_SEAT/2)+1.5f, 0);
		}
		else if ( gameObject.name.Contains("2") )
		{
			if ( SeatIndex < MAX_SEAT/2 )
				return transform.position + new Vector3 (-1.3f-0.3f*(SeatIndex%(MAX_SEAT/2)), -SeatIndex%(MAX_SEAT/2)+1.3f, 0);
			else
				return transform.position + new Vector3 (1.3f-0.3f*(SeatIndex%(MAX_SEAT/2)), -SeatIndex%(MAX_SEAT/2)+1.3f, 0);
		}
		else if ( gameObject.name.Contains("3") )
		{
			if ( SeatIndex < MAX_SEAT/2 )
				return transform.position + new Vector3 (-1.3f+0.3f*(SeatIndex%(MAX_SEAT/2)), -SeatIndex%(MAX_SEAT/2)+1.3f, 0);
			else
				return transform.position + new Vector3 (1.3f+0.3f*(SeatIndex%(MAX_SEAT/2)), -SeatIndex%(MAX_SEAT/2)+1.3f, 0);
		}
		else
		{
			if ( SeatIndex < MAX_SEAT/2 )
				return transform.position + new Vector3 (-1.7f+0.3f*(SeatIndex%(MAX_SEAT/2)), -SeatIndex%(MAX_SEAT/2)+1.3f, 0);
			else
				return transform.position + new Vector3 (1.5f+0.3f*(SeatIndex%(MAX_SEAT/2)), -SeatIndex%(MAX_SEAT/2)+1.3f, 0);
		}
	}
	
	public bool IsTaken()
	{
		for ( int i= 0 ; i < MAX_SEAT ; i++ )
		{
			if ( seatState[i] != SEAT_STATE.EMPTY )
				return true;
		}
		return false;
	}
	
	public int GetAvailableSeatIndex()
	{
		for ( int i= 0 ; i < MAX_SEAT ; i++ )
		{
			if ( seatState[i] == SEAT_STATE.EMPTY )
				return i;
		}
		return -1;
	}
	
	public int GetNumOfSeatLeft()
	{
		int NumOfSeatLeft = 0;
		for ( int i= 0 ; i < MAX_SEAT ; i++ )
		{
			if ( seatState[i] == SEAT_STATE.EMPTY )
				NumOfSeatLeft++;
		}
		return NumOfSeatLeft;
	}

	public void DestroyEatingSprite()
	{
		if ( EatingSpriteObj != null )
			Destroy (EatingSpriteObj);
		EatingSpriteObj = null;
	}
}
