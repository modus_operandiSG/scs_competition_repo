﻿using UnityEngine;
using System.Collections;

public class IceKacangIngredientIconController : MonoBehaviour 
{
	private ICE_KACANG_INGREDIENT_TYPE Type;
	public GameObject GlowPrefab;
	private GameObject GlowObj;

	public void Init(ICE_KACANG_INGREDIENT_TYPE Ingredient_Type)
	{
		Type = Ingredient_Type;
	}

	public ICE_KACANG_INGREDIENT_TYPE GetIngredientType()
	{
		return Type;
	}

	public void CreateGlow()
	{
		Vector3 tempPosition = new Vector3 (gameObject.transform.position.x,
		                                    gameObject.transform.position.y,
		                                    GlowPrefab.transform.position.z);
		GlowObj = (GameObject)Instantiate( GlowPrefab,
		                                   tempPosition,
		                                   gameObject.transform.rotation);
	}

	public void DestroySelf()
	{
		if ( GlowObj )
			Destroy (GlowObj);
		Destroy (gameObject);
	}

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
	
}
