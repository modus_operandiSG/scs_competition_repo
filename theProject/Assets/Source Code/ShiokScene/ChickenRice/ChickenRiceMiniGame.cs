﻿using UnityEngine;
using System.Collections;

public enum CHICKEN_TYPE
{
	ROAST,
	STEAM,
}

public class ChickenRiceMiniGame : MonoBehaviour 
{	
	public GameObject backGroundObj;
	public GameObject RiceSetObj;
	public GameObject TickObj1;
	public GameObject TickObj2;
	public GameObject SetIndicatorObj;
	public GameObject[] SetIndicatorPrefabs;

	//Dotted Line
	public GameObject[] DottedLinePrefabs;
	private ArrayList DottedLineObjs;

	//Chicken Piece
	public GameObject[] ChickenPiecePrefabs;
	public GameObject[] ChickenPrefabs;
	private GameObject[] ChickenPieceObjs;
	private GameObject CurrentMovingObj;

	//Chicken Piece Cut
	public GameObject[] ChickenPieceCutPrefabs;
	private ArrayList ChickenPieceCutObjs;

	private const int NUM_OF_PIECES_OF_CHICKEN = 5;
	private const int NUM_OF_CHICKEN_TYPE = 2;
	private const float OFFSET = 0.3f;
	private CHICKEN_TYPE Type;

	private float fMiniGameFinished_Time;
	private const float MINIGAME_FINISH_DELAY = 1.0f;
	
	// Use this for initialization
	void Start () 
	{
		// Randomly choose a chicken set
		Type = (CHICKEN_TYPE)Random.Range (0, NUM_OF_CHICKEN_TYPE);

		RiceSetObj = (GameObject)Instantiate(RiceSetObj,
		                                     RiceSetObj.transform.position,
		                                     RiceSetObj.transform.rotation);
		backGroundObj = (GameObject)Instantiate(backGroundObj,
		                                        backGroundObj.transform.position,
		                                        backGroundObj.transform.rotation);
		SetIndicatorObj = (GameObject)Instantiate(SetIndicatorPrefabs[(int)Type],
		                                          SetIndicatorPrefabs[(int)Type].transform.position,
		                                          SetIndicatorPrefabs[(int)Type].transform.rotation);

		//init chicken
		ChickenPieceObjs = new GameObject[NUM_OF_PIECES_OF_CHICKEN];
		for ( int i = 0 ; i < NUM_OF_PIECES_OF_CHICKEN ; i++ )
		{
			ChickenPieceObjs[i] = (GameObject)Instantiate(ChickenPiecePrefabs[(int)Type*NUM_OF_PIECES_OF_CHICKEN+i],
			                                              ChickenPiecePrefabs[(int)Type*NUM_OF_PIECES_OF_CHICKEN+i].transform.position,
			                                              ChickenPiecePrefabs[(int)Type*NUM_OF_PIECES_OF_CHICKEN+i].transform.rotation);
			// The first piece of chicken, left side don't need cut
			if ( i == 0 )
				ChickenPieceObjs[i].GetComponent<ChickenPieceController>().Init(true,false);
			// The last piece of chicken, right side don't need cut
			else if ( i == NUM_OF_PIECES_OF_CHICKEN-1 )
				ChickenPieceObjs[i].GetComponent<ChickenPieceController>().Init(false,true);
			//other pieces both side need
			else
				ChickenPieceObjs[i].GetComponent<ChickenPieceController>().Init(false,false);
		}

		//init dotted line
		DottedLineObjs = new ArrayList();
		for ( int i = 0 ; i < DottedLinePrefabs.Length ; i++ )
		{
			GameObject newDottedLineObj = (GameObject)Instantiate(DottedLinePrefabs[i],
			                                                      DottedLinePrefabs[i].transform.position,
			                                                      DottedLinePrefabs[i].transform.rotation);
			newDottedLineObj.GetComponent<DottedLineController>().Init(i);
			DottedLineObjs.Add(newDottedLineObj);
		}

		ChickenPieceCutObjs = new ArrayList ();
		fMiniGameFinished_Time = -1;
	}

	void SplitChicken(int ID)
	{
		ID = ID + 1;
		//split chicken to left
		for ( int i = 0 ; i < ID; i++ )
		{
			ChickenPieceObjs[i].transform.position += new Vector3(-OFFSET,0,0);
		}

		//split chicken to right
		for ( int i = ID ; i < NUM_OF_PIECES_OF_CHICKEN ; i++ )
		{
			ChickenPieceObjs[i].transform.position += new Vector3(OFFSET,0,0);
		}

		//split dotted line
		for ( int i = 0 ; i < DottedLineObjs.Count; i++ )
		{
			DottedLineController dottedLineController = ((GameObject)DottedLineObjs[i]).GetComponent<DottedLineController>();

			if ( dottedLineController.GetID() < ID )
				((GameObject)DottedLineObjs[i]).transform.position += new Vector3(-OFFSET,0,0);
			else
				((GameObject)DottedLineObjs[i]).transform.position += new Vector3(OFFSET,0,0);
		}
	}

	public bool IsMiniGameFinish()
	{
		if (fMiniGameFinished_Time > 0)
			return true;
		return false;
	}

	private void DestroyMiniGame()
	{
		Destroy (backGroundObj);
		Destroy (RiceSetObj);
		Destroy (TickObj1);
		Destroy (TickObj2);
		Destroy (SetIndicatorObj);
		foreach(GameObject obj in DottedLineObjs)
		{
			Destroy(obj);
		}
		foreach(GameObject obj in ChickenPieceObjs)
		{
			Destroy(obj);
		}
		foreach(GameObject obj in ChickenPieceCutObjs)
		{
			Destroy(obj);
		}
		Destroy (gameObject);
	}

	// Update is called once per frame
	void Update () 
	{
		//update dotted line
		foreach(GameObject obj in DottedLineObjs)
		{
			DottedLineController dottedLineController = obj.GetComponent<DottedLineController>();
			if ( dottedLineController.GetIsCut() )
			{
				SplitChicken(dottedLineController.GetID());
				ChickenPieceObjs[dottedLineController.GetID()].GetComponent<ChickenPieceController>().IsRightSideCut = true;
				ChickenPieceObjs[dottedLineController.GetID()+1].GetComponent<ChickenPieceController>().IsLeftSideCut = true;
				DottedLineObjs.Remove(obj);
				Destroy(obj);
				//if no dotted line left show tick
				if ( DottedLineObjs.Count == 0 )
				{
					TickObj1 = (GameObject)Instantiate(TickObj1,
					                                   TickObj1.transform.position,
					                                   TickObj1.transform.rotation);
				}
				break;
			}
		}

		//update chicken piece
		for ( int i = 0 ; i < NUM_OF_PIECES_OF_CHICKEN ; i++ )
		{
			ChickenPieceController chickenPieceController = ChickenPieceObjs[i].GetComponent<ChickenPieceController>();
			//get mouse position in world point
			Vector3 tempPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			tempPosition.z = ChickenPieceObjs[i].transform.position.z;
			//select a chicken piece
			if ( Input.GetMouseButtonDown(0) && ChickenPieceObjs[i].collider.bounds.Contains(tempPosition) )
			{
				CurrentMovingObj = ChickenPieceObjs[i];
			}
			//move a chicken piece
			if ( Input.GetMouseButton(0) && CurrentMovingObj == ChickenPieceObjs[i] )
			{
				if ( chickenPieceController.IsCut() )
				{
					ChickenPieceObjs[i].transform.position = tempPosition;
				}
			}
			//deselect a chicken piece
			else if ( Input.GetMouseButtonUp(0) )
			{
				if ( chickenPieceController.IsInsideRiceSet )
				{
					ChickenPieceObjs[i].renderer.enabled = false;
					ChickenPieceObjs[i].collider.enabled = false;
				}
				CurrentMovingObj = null;
			}
			//replace chicken piece
			if ( ChickenPieceObjs[i].renderer.enabled == false && !chickenPieceController.IsPlaced )
			{
				GameObject newChickenPieceCutObj = (GameObject)Instantiate(ChickenPieceCutPrefabs[(int)Type*NUM_OF_PIECES_OF_CHICKEN+i],
				                                                           ChickenPieceCutPrefabs[(int)Type*NUM_OF_PIECES_OF_CHICKEN+i].transform.position,
				                                                           ChickenPieceCutPrefabs[(int)Type*NUM_OF_PIECES_OF_CHICKEN+i].transform.rotation);
				ChickenPieceCutObjs.Add(newChickenPieceCutObj);
				ChickenPieceObjs[i].GetComponent<ChickenPieceController>().IsPlaced = true;
			}
		}
		//if all chicken piece is placed, end this minigame
		if ( ChickenPieceCutObjs.Count >= NUM_OF_PIECES_OF_CHICKEN )
		{
			if ( !IsMiniGameFinish() )
			{
				//all chicken piece is placed show 2nd tick
				TickObj2 = (GameObject)Instantiate(TickObj2,
				                                   TickObj2.transform.position,
				                                   TickObj2.transform.rotation);
				fMiniGameFinished_Time = Time.time;
			}
			if ( Time.time > fMiniGameFinished_Time+MINIGAME_FINISH_DELAY )
			{
				DestroyMiniGame();
			}
		}
	}
	
}
