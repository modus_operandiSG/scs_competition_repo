﻿using UnityEngine;
using System.Collections;

public class ChickenPieceController : MonoBehaviour 
{
	public bool IsLeftSideCut;
	public bool IsRightSideCut;
	public bool IsPlaced;
	public bool IsInsideRiceSet;

	public void Init(bool isLeftSideCut, bool isRightSideCut)
	{
		IsLeftSideCut = isLeftSideCut;
		IsRightSideCut = isRightSideCut;
		IsInsideRiceSet = false;
		IsPlaced = false;
	}

	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
	}

	public bool IsCut()
	{
		if ( IsLeftSideCut && IsRightSideCut )
			return true;
		return false;
	}

	void OnTriggerEnter(Collider col)
	{
		if ( col.gameObject.name.Contains("RiceSet") )
		{
			IsInsideRiceSet = true;
		}
	}

	void OnTriggerExit(Collider col)
	{
		if ( col.gameObject.name.Contains("RiceSet") )
		{
			IsInsideRiceSet = false;
		}
	}
	
}
