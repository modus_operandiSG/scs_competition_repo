﻿using UnityEngine;
using System.Collections;

public class NasiLemakIngredientController : MonoBehaviour 
{
	public GameObject IngredientHint;
	public bool IsIngredientPlaced;

	public void Init(GameObject ingredientHint)
	{
		IngredientHint = ingredientHint;
		IsIngredientPlaced = false;
	}

	// Use this for initialization
	void Start () 
	{
	}

	// Update is called once per frame
	void Update () 
	{
	}
}
