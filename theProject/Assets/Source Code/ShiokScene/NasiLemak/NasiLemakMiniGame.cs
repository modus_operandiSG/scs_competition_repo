﻿using UnityEngine;
using System.Collections;

public enum NASILEMAK_SET_TYPE
{
	CHICKEN_WING,
	FISH,
	OTAH,
}

public class NasiLemakMiniGame : MonoBehaviour 
{
	// basic objs
	public GameObject BackGroundObj;
	public GameObject PlateObj;
	public GameObject TickObj;

	// Set Indicatior
	public GameObject SetIndicatorObj;
	public GameObject[] SetIndicatorPrefabs;

	// Ingredient
	public GameObject[] IngredientPrefabs;
	public GameObject[] IngredientHintPrefabs;
	private GameObject[] IngredientObjs;
	private ArrayList IngredientHintObjs;
	
	private GameObject CurrentMovingObj;


	private const int NUM_OF_INGREDIENT_NEEDED = 5;
	private const int NUM_OF_INGREDIENT = 7;
	private const int NUM_OF_SET = 3;
	private NASILEMAK_SET_TYPE Type;
	private float fMiniGameFinished_Time;
	private const float MINIGAME_FINISH_DELAY = 1.0f;

	// Use this for initialization
	void Start () 
	{
		Type = (NASILEMAK_SET_TYPE)Random.Range (0,NUM_OF_SET);

		// init basic objs
		PlateObj = (GameObject)Instantiate(PlateObj,
		                       PlateObj.transform.position,
		                       PlateObj.transform.rotation);
		BackGroundObj = (GameObject)Instantiate(BackGroundObj,
		                                        BackGroundObj.transform.position,
		                                        BackGroundObj.transform.rotation);
		SetIndicatorObj = (GameObject)Instantiate(SetIndicatorPrefabs[(int)Type],
		                                          SetIndicatorPrefabs[(int)Type].transform.position,
		                                          SetIndicatorPrefabs[(int)Type].transform.rotation);

		// init ingredient objs
		IngredientObjs = new GameObject[NUM_OF_INGREDIENT];
		IngredientHintObjs = new ArrayList ();
		for ( int i = 0 ; i < NUM_OF_INGREDIENT ; i++ )
		{
			IngredientObjs[i] = (GameObject)Instantiate(IngredientPrefabs[i],
			                                            IngredientPrefabs[i].transform.position,
			                                            IngredientPrefabs[i].transform.rotation);
		}

		// init needed ingredient objs
		for ( int i = 0 ; i < NUM_OF_INGREDIENT_NEEDED ; i++ )
		{
			// is it last obj ?
			int index = (i == NUM_OF_INGREDIENT_NEEDED-1 ? i+(int)Type : i);
			// create hint obj
			GameObject newHintObj = (GameObject)Instantiate(IngredientHintPrefabs[index],
			                                                IngredientHintPrefabs[index].transform.position,
			                                                IngredientHintPrefabs[index].transform.rotation);
			IngredientHintObjs.Add(newHintObj);
			// set alpha value
			newHintObj.GetComponent<SpriteRenderer>().color = new Vector4(1.0f,1.0f,1.0f,0.5f);
			// assign hint obj to ingredient obj
			IngredientObjs[index].GetComponent<NasiLemakIngredientController>().Init(newHintObj);
		}

		fMiniGameFinished_Time = -1;
	}

	public bool IsMiniGameFinish()
	{
		if (fMiniGameFinished_Time > 0)
			return true;
		return false;
	}

	private void DestroyMiniGame()
	{
		Destroy(BackGroundObj);
		Destroy(PlateObj);
		Destroy(TickObj);
		Destroy(SetIndicatorObj);
		foreach(GameObject obj in IngredientObjs)
		{
			Destroy(obj);
		}
		foreach(GameObject obj in IngredientHintObjs)
		{
			Destroy(obj);
		}
		Destroy(gameObject);
	}
	
	// Update is called once per frame
	void Update () 
	{

		if ( !IsMiniGameFinish() )
		{
			foreach(GameObject obj in IngredientObjs)
			{
				NasiLemakIngredientController nasiLemakIngredientController = obj.GetComponent<NasiLemakIngredientController>();
				// is this indredient placed ?
				if ( !nasiLemakIngredientController.IsIngredientPlaced )
				{
					// get mouse position
					Vector3 tempPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
					// set mouse position z value the same as the obj
					tempPosition.z = obj.transform.position.z;
					// select the obj
					if ( Input.GetMouseButtonDown(0) && obj.collider.bounds.Contains(tempPosition) )
					{
						CurrentMovingObj = obj;
					}
					// moving the obj
					if ( Input.GetMouseButton(0) && CurrentMovingObj == obj)
					{
						obj.transform.position = tempPosition;
					}
					// release the obj
					else if ( Input.GetMouseButtonUp(0) )
					{
						SphereCollider collider = obj.transform.GetComponent<SphereCollider>();
						CurrentMovingObj = null;
						// is the hint obj exist ?
						if ( nasiLemakIngredientController.IngredientHint )
						{
							float sqrDistance = (obj.transform.position - nasiLemakIngredientController.IngredientHint.transform.position).sqrMagnitude;
							// are they most in the same place ?
							if ( sqrDistance < collider.radius*collider.radius)
							{
								// set obj to the same place as hint obj
								obj.transform.position = nasiLemakIngredientController.IngredientHint.transform.position;
								// remove hint obj
								IngredientHintObjs.Remove(nasiLemakIngredientController.IngredientHint);
								Destroy(nasiLemakIngredientController.IngredientHint);
								// is all obj placed ?
								if ( IngredientHintObjs.Count == 0 )
								{
									// timer start
									fMiniGameFinished_Time = Time.time;
									// show the tick
									TickObj = (GameObject)Instantiate(TickObj,
									                                  TickObj.transform.position,
									                                  TickObj.transform.rotation);
								}
								nasiLemakIngredientController.IsIngredientPlaced = true;
							}
						}
					}
				}
			}
		}
		else
		{
			// delay time over
			if ( Time.time > fMiniGameFinished_Time+MINIGAME_FINISH_DELAY )
			{
				DestroyMiniGame();
			}
		}
	}
}
