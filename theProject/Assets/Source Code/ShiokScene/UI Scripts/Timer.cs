﻿using UnityEngine;
using System.Collections;

public class Timer : MonoBehaviour 
{
	public float timer = 180.0f;
	public bool timerStarted = false;

	//font manipulation
	public GUISkin guiSkin;
	//public Texture2D timerBg;
	GUIStyle largeFont;

	private int screenW = Screen.width;
	private int screenH = Screen.height;

	// Use this for initialization
	void Start () 
	{
		if ( timerStarted == false)
		{
			timerStarted = true;
		}
		else
		{
			timerStarted = false;
		}

		largeFont = new GUIStyle ();
		largeFont.fontSize = 33;
		largeFont.normal.textColor = Color.white;
	}
	
	// Update is called once per frame
	void Update () 
	{
		//testing timer
		if (Input.GetKeyDown (KeyCode.T)) 
		{
			if ( timerStarted == false)
			{
				timerStarted = true;
			}
			else
			{
				timerStarted = false;
			}
		}

		if (timerStarted) 
		{
			timer -= Time.deltaTime;
			
			if ( timer < 0)
			{
				timerStarted = false;
				timer = 0.0f;
				FadeTransition.LoadLevel ("GameOverScene",0.5f,1,Color.green);
			}
		}
	}

	public void TimeOfDay()
	{
		if (timer <= 180.0f || timer > 90.0f)
		{
			// Morning
		}
		else if ( timer <= 90.0f || timer > 60.0f)
		{
			// Afternoon
		}
		else if ( timer <= 60.0f)
		{
			// Night
		}
	}

	void OnGUI()
	{
		int minutes = Mathf.FloorToInt (timer / 60F);
		int seconds = Mathf.FloorToInt (timer - minutes * 60);

		//To give the format of 0:00 E.G: 3 Minutes -> 3:00
		string niceTime = string.Format ("{0:0}:{1:00}", minutes, seconds);

		// for custom fonts
		GUI.skin = guiSkin;

		GUI.depth = -10;
		// Time
		GUI.Label (new Rect( (Screen.width*209) / 1920, (Screen.height*1023) / 1080 ,(Screen.width*120) / 1920,(Screen.height*50) / 1080),niceTime, largeFont);
	}
}