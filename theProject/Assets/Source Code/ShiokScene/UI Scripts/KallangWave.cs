﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class KallangWave : MonoBehaviour 
{

	private LineRenderer line;
	private bool isMousePressed;
	private List<Vector3> pointsList;
	private Vector3 mousePos;

	// Structure for line points
	struct myLine
	{
		public Vector3 StartPoint;
		public Vector3 EndPoint;
	};
	//    -----------------------------------    
	void Awake()
	{
		// Create line renderer component and set its property
		line = gameObject.AddComponent<LineRenderer>();
		line.material =  new Material(Shader.Find("Particles/Additive"));
		line.SetVertexCount(0);
		line.SetWidth(0.5f,0.5f);
		line.SetColors(Color.green, Color.green);
		line.useWorldSpace = true;    
		isMousePressed = false;
		pointsList = new List<Vector3>();
	}
	//    -----------------------------------    
	void Update () 
	{
		GameObject getTimer = GameObject.Find ("Pause");
		Timer getTiming = getTimer.GetComponent <Timer> ();

		GameObject collider2 = GameObject.Find ("WaveCollider2");
		BoxCollider waveCol2 = collider2.GetComponent <BoxCollider> ();
		
		GameObject collider3 = GameObject.Find ("WaveCollider3");
		BoxCollider waveCol3 = collider3.GetComponent <BoxCollider> ();
		
		GameObject collider4 = GameObject.Find ("WaveCollider4");
		BoxCollider waveCol4 = collider4.GetComponent <BoxCollider> ();

		if ((getTiming.timer <= 60.0f) && (getTiming.timer >= 53.0f))
		{
			collider.enabled = true;
		}
		else if (getTiming.timer < 53.0f)
		{
			collider.enabled = false;
		}
		
		// If mouse button down, remove old line and set its color to green
		if(Input.GetMouseButtonDown(0))
		{
			isMousePressed = true;
			line.SetVertexCount(0);
			pointsList.RemoveRange(0,pointsList.Count);
			line.SetColors(Color.blue,Color.green);
		}
		else if(Input.GetMouseButtonUp(0))
		{
			isMousePressed = false;
			waveCol2.collider.enabled = false;
			waveCol3.collider.enabled = false;
			waveCol4.collider.enabled = false;
		}
		// Drawing line when mouse is moving(presses)
		if(isMousePressed)
		{
			mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			mousePos.z=0;
			if (!pointsList.Contains (mousePos)) 
			{
				pointsList.Add (mousePos);
				line.SetVertexCount (pointsList.Count);
				line.SetPosition (pointsList.Count - 1, (Vector3)pointsList [pointsList.Count - 1]);
				if(isLineCollidedWithOtherObject())
				{
					//isMousePressed = false;
					line.SetColors(Color.red, Color.red);

				}
			}
		}
	}

	public bool isLineCollidedWithOtherObject()
	{
		GameObject collider2 = GameObject.Find ("WaveCollider2");
		BoxCollider waveCol2 = collider2.GetComponent <BoxCollider> ();

		GameObject collider3 = GameObject.Find ("WaveCollider3");
		BoxCollider waveCol3 = collider3.GetComponent <BoxCollider> ();

		GameObject collider4 = GameObject.Find ("WaveCollider4");
		BoxCollider waveCol4 = collider4.GetComponent <BoxCollider> ();
		
		RaycastHit hit;
		Ray ray = Camera.main.ScreenPointToRay(Camera.main.WorldToScreenPoint(pointsList[pointsList.Count-1]));
		if(Physics.Raycast(ray,out hit))
		{
			if (hit.collider.name.Equals("WaveCollider1"))
			{
				waveCol2.collider.enabled = true;
				return false;
			}
			
			if (hit.collider.name.Equals("WaveCollider2"))
			{
				waveCol3.collider.enabled = true;
				return false;
			}

			if (hit.collider.name.Equals("WaveCollider3"))
			{
				waveCol4.collider.enabled = true;
				return false;
			}

			if (hit.collider.name.Equals("WaveCollider4"))
			{
				return true;
			}
		}
		return false;
	} 
}
