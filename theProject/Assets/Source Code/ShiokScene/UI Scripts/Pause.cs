﻿ using UnityEngine;
using System.Collections;

public class Pause : MonoBehaviour 
{
	public Texture2D pauseMenu;
	public Texture2D[] pausing;
	public int isPause = 0;

	public ShiokBar sBar;

	public Vector3 mousePos;

	public Vector2 startTouch = Vector2.zero;

	// Use this for initialization
	void Start () 
	{

	}

	void OnGUI()
	{
		GameObject retryCollider = GameObject.Find ("PauseRetry");
		BoxCollider pauseRetryBtn = retryCollider.GetComponent <BoxCollider> ();

		GUI.DrawTexture (new Rect ((Screen.width*1) / 1920, (Screen.height*968) / 1080, (Screen.width*370) / 1920, (Screen.height*115) / 1080), pausing[isPause]);

		if (pauseRetryBtn.collider.enabled == true)
		{
			GUI.depth = -11;
			GUI.DrawTexture (new Rect ((Screen.width * 1) / 1920, (Screen.height * 1) / 1080, (Screen.width * 1920) / 1920, (Screen.height * 1080) / 1080), pauseMenu);
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		PauseMenuFuntions ();
	}

	void OnMouseDown()
	{
		GameObject sound = GameObject.Find ("AudioManager");
		SoundManager pauseclick = sound.GetComponent<SoundManager> ();

		pauseclick.StartButton ();

		doPauseToggle ();
	}

	void doPauseToggle()
	{
		GameObject retryCollider = GameObject.Find ("PauseRetry");
		BoxCollider pauseRetryBtn = retryCollider.GetComponent <BoxCollider> ();
		
		GameObject pauseMMCollider = GameObject.Find ("PauseMainMenu");
		BoxCollider pauseMMBtn = pauseMMCollider.GetComponent <BoxCollider> ();

		GameObject pauseResumeCollider = GameObject.Find ("PauseResume");
		BoxCollider pauseResumeBtn = pauseResumeCollider.GetComponent <BoxCollider> ();

		if(isPause == 0)
		{
			pauseRetryBtn.collider.enabled = true;

			pauseMMBtn.collider.enabled = true;

			pauseResumeBtn.collider.enabled = true;

			isPause = 1;

			sBar.enabled = false;

			pauseGame();
		}
	}

	void pauseGame()
	{
		Time.timeScale = 0;
	}

	void unPauseGame()
	{
		Time.timeScale = 1;
	}

	void PauseMenuFuntions()
	{

		GameObject retryCollider = GameObject.Find ("PauseRetry");
		BoxCollider pauseRetryBtn = retryCollider.GetComponent <BoxCollider> ();
		
		GameObject pauseMMCollider = GameObject.Find ("PauseMainMenu");
		BoxCollider pauseMMBtn = pauseMMCollider.GetComponent <BoxCollider> ();
		
		GameObject pauseResumeCollider = GameObject.Find ("PauseResume");
		BoxCollider pauseResumeBtn = pauseResumeCollider.GetComponent <BoxCollider> ();

		foreach (Touch touch in Input.touches) 
		{
			startTouch = touch.position;
			Ray ray = Camera.main.ScreenPointToRay(touch.position);
			RaycastHit hit;
			
			if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began) 
			{
				if(Physics.Raycast(ray, out hit))
				{	
					GameObject sound = GameObject.Find ("AudioManager");
					SoundManager pauseMenuS = sound.GetComponent<SoundManager> ();

					// Pause main menu Button Interface
					GameObject pauseMMenu = GameObject.Find ("PauseMainMenu");
					if(hit.collider.gameObject == pauseMMenu)
					{
						unPauseGame();
						pauseMenuS.MenuButtons();
						//Loads Main menu.
						FadeTransition.LoadLevel("StartMenuScene", 0.5f,0.5f,Color.black);
					}
					
					//Retry game button
					GameObject gameMusic = GameObject.Find ("GameMusic");
					GameObject pointsCarry = GameObject.Find ("PointsCarrying");
					GameObject pauseRetryBtnHit = GameObject.Find ("PauseRetry");
					if(hit.collider.gameObject == pauseRetryBtnHit)
					{
						unPauseGame();
						pauseMenuS.MenuButtons();
						Destroy (gameMusic);
						Destroy (pointsCarry);
						//Retry game
						FadeTransition.LoadLevel("ShiokScene",0.5f,0.5f,Color.black);
						
					}
					
					//Resume Page
					GameObject resume = GameObject.Find ("PauseResume");
					if(hit.collider.gameObject == resume)
					{
						pauseMenuS.MenuButtons();

						isPause = 0;
						
						pauseRetryBtn.collider.enabled = false;
						
						pauseMMBtn.collider.enabled = false;
						
						pauseResumeBtn.collider.enabled = false;
						
						sBar.enabled = true;

						unPauseGame();
					}
				}
			}
		}
	}
}
