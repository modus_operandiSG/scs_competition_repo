﻿using UnityEngine;
using System.Collections;

public class LevelUpPopout : MonoBehaviour 
{
	float fLife_Time;
	const float LEVELUP_DURATION = 1.0f;

	IEnumerator WaitPopOut()
	{
		renderer.enabled = true;

		fLife_Time = Time.time;

//		GameObject sound = GameObject.Find ("AudioManager");
//		SoundManager levelSound = sound.GetComponent <SoundManager> ();
//		
//		levelSound.GameLevelUp ();

		yield return new WaitForSeconds (3.0f);

		renderer.enabled = false;
	}

	// Use this for initialization
	void Start () 
	{

	}

	void UpdateColorAlpha()
	{
		Color color = renderer.material.color;
		color.a = (fLife_Time + LEVELUP_DURATION - Time.time)/LEVELUP_DURATION;
		renderer.material.color = color;
	}

	
	// Update is called once per frame
	void Update () 
	{
		UpdateColorAlpha ();

		GameObject go = GameObject.Find ("ShiokBarUI");
		ShiokBar shiokBar = go.GetComponent <ShiokBar> ();

		if(shiokBar.lvl == 2 || shiokBar.lvl == 3)
		{
			if (shiokBar.curShiok <= 1)
			{
				StartCoroutine(WaitPopOut());
			}
		}
	}
}
