﻿using UnityEngine;
using System.Collections;

public class GameMusic : MonoBehaviour 
{
	void Awake () 
	{
		GameObject menuMusic = GameObject.Find ("MenuMusic");
		if(menuMusic)
		{
			// kill game music
			Destroy(menuMusic);
		}
		
		DontDestroyOnLoad (gameObject);
	}
}
