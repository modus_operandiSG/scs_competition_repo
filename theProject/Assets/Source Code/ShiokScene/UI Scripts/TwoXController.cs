﻿using UnityEngine;
using System.Collections;

public class TwoXController : MonoBehaviour 
{
	public PointsCarry pp;
	public bool activated = false;
	public bool displaySkill = false;

	public Texture2D item1DisplayName;

	IEnumerator DisplaySkillName()
	{
		yield return new WaitForSeconds (2.0f);

		displaySkill = false;
	}

	IEnumerator Active()
	{
		GameObject go = GameObject.Find ("ShiokBarUI");
		ShiokBar shiokBar = go.GetComponent <ShiokBar> ();

		pp.points += (shiokBar.combo * 10);
		shiokBar.curShiok += (shiokBar.combo * 10);

		activated = true;

		yield return new WaitForSeconds (5.0f);

		activated = false;

	}
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		GameObject go = GameObject.Find ("ShiokBarUI");
		GameObject go2 = GameObject.Find ("PointsCarrying");
		ShiokBar shiokBar = go.GetComponent <ShiokBar> ();
		PointsCarry moneyNow = go2.GetComponent <PointsCarry> ();

		if ( shiokBar.lvl == 2 && shiokBar.buttonOpen && moneyNow.money >= 50)
		{ 
			collider.enabled = true;
		}
		else
		{
			collider.enabled = false;
		}

		if (displaySkill == true)
		{
			StartCoroutine(DisplaySkillName());
		}
	}

	void OnMouseDown()
	{
		GameObject go = GameObject.Find ("ShiokBarUI");
		ShiokBar shiokBar = go.GetComponent <ShiokBar> ();

		Debug.Log("test");

		StartCoroutine (Active());

		collider.enabled = false;
		
		shiokBar.buttonOpen = false;

		displaySkill = true;

		go.GetComponent<MoneyManager> ().BuyTwoTimes ();
	}
}
