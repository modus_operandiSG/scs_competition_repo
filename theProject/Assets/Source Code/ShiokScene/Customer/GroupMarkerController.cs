﻿using UnityEngine;
using System.Collections;

public class GroupMarkerController : MonoBehaviour 
{
	public Sprite[] theSprites;
	private GameObject theCustomer;
	// Use this for initialization
	void Start () 
	{
		gameObject.transform.position = theCustomer.GetComponent<CustomerController> ().GetSideHeadPosition ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		gameObject.transform.position = theCustomer.GetComponent<CustomerController> ().GetSideHeadPosition ();
	}

	public void Set(GameObject Customer,int GroupID)
	{
		theCustomer = Customer;
		gameObject.GetComponent<SpriteRenderer> ().sprite = theSprites [GroupID];
	}
}
