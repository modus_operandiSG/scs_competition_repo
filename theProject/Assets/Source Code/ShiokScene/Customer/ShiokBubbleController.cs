﻿ using UnityEngine;
using System.Collections;

public class ShiokBubbleController : MonoBehaviour 
{
	float fLife_Time;
	const float SHIOKBUBBLE_DURATION = 2.0f;
	const float VELOCITY_X_MIN = -2.0f;
	const float VELOCITY_X_MAX = 2.0f;
	const float VELOCITY_Y_MIN = 3.0f;
	const float VELOCITY_Y_MAX = 6.0f;
	const float ACCELERATE_Y = -6.0f;
	Vector3 velocity;
	Vector3 accelerate;

	// Use this for initialization
	void Start () 
	{
		fLife_Time = Time.time;
		velocity = new Vector3 (Random.Range (VELOCITY_X_MIN,VELOCITY_X_MAX),Random.Range (VELOCITY_Y_MIN,VELOCITY_Y_MAX),0);
		accelerate = new Vector3 (0,ACCELERATE_Y,0);

		GameObject sound = GameObject.Find ("AudioManager");
		SoundManager pointsSound = sound.GetComponent <SoundManager> ();

		pointsSound.GameShiokPoints ();
	}

	void UpdateColorAlpha()
	{
		Color color = renderer.material.color;
		color.a = (fLife_Time + SHIOKBUBBLE_DURATION - Time.time)/SHIOKBUBBLE_DURATION;
		renderer.material.color = color;
	}

	void UpdatePosition()
	{
		velocity += accelerate * Time.deltaTime;
		gameObject.transform.position += velocity*Time.deltaTime;
	}
	
	// Update is called once per frame
	void Update () 
	{
		UpdateColorAlpha ();
		UpdatePosition ();

		GameObject go = GameObject.Find ("ShiokBarUI");
		ShiokBar shiokBar = go.GetComponent <ShiokBar> ();

		GameObject go2 = GameObject.Find ("PointsCarrying");
		PointsCarry pointsS = go2.GetComponent <PointsCarry> ();

		if ( Time.time > fLife_Time + SHIOKBUBBLE_DURATION )
		{
			shiokBar.curShiok += (shiokBar.combo * 5); // increase bar counter
			pointsS.points += (shiokBar.combo * 5); // increase points
			
			// Conditions for shiok bar
			if ((shiokBar.curShiok > shiokBar.maxShiok) && (shiokBar.lvl == 3))
			{
				shiokBar.curShiok = 1000;
			}
			else if (shiokBar.curShiok > shiokBar.maxShiok)
			{
				shiokBar.curShiok = 0;
				shiokBar.lvl ++;
				shiokBar.lvlup ++;
			}
			
			// set level amount
			if ( shiokBar.lvl > 3)
			{
				shiokBar.lvl = 3;
				shiokBar.lvlup = 2;
			}
			
			if ((shiokBar.lvl == 3 && shiokBar.curShiok == 1000)) // Key to activate shiok mode
			{
				if (shiokBar.shiokMode == false)
				{
					shiokBar.shiokMode = true;
				}
			}

			Destroy(gameObject);
		}
	}
}
