﻿using UnityEngine;
using System.Collections;

public class PatienceIndicatorController : MonoBehaviour 
{
	public Sprite[] theSprites;
	public float patienceValue;
	public float maxPatienceValue;
	public GameObject customer;
	//public bool addGameOver = false;

	public void Init(GameObject Customer,float MaxPatienceValue)
	{
		customer = Customer;
		maxPatienceValue = MaxPatienceValue;
		gameObject.GetComponent<SpriteRenderer> ().sprite = theSprites [0];
		patienceValue = MaxPatienceValue;
	}

	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void Update () 
	{
		GameObject go = GameObject.Find ("ShiokBarUI");
		ShiokBar shiokBar = go.GetComponent <ShiokBar> ();

		patienceValue -= Time.deltaTime;
		if ( patienceValue > 0 )
		{
			gameObject.GetComponent<SpriteRenderer> ().sprite = theSprites [(int)((maxPatienceValue-patienceValue)/maxPatienceValue*theSprites.Length)];
		}
		gameObject.transform.position = customer.GetComponent<CustomerController> ().GetSideHeadPosition () + new Vector3(-0.08f,0.3f,-1.0f);

		if (shiokBar.gameOverText >= 15)
		{
			FadeTransition.LoadLevel("GameOverScene",0.5f,1.0f,Color.red);
			
			shiokBar.gameOverText = 15;
		}

		if (patienceValue < 0)
		{
			shiokBar.combo = 1;
			shiokBar.comboImage = 0;
			shiokBar.gameOverText += 1;
			CustomerController customerController = customer.GetComponent<CustomerController>();
			customerController.StopMoving();
			customerController.currentState = CUSTOMER_AI_STATE.LEAVE;
			customerController.DestroyPatienceIndicator();
			customerController.DestroyGroupMarker();
		}
	}
}
