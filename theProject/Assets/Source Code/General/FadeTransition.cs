﻿using UnityEngine;
using System.Collections;

public class FadeTransition : MonoBehaviour 
{
	private static FadeTransition m_Instance = null;
	private Material m_Material = null;
	private string m_LevelName = "";
	private bool m_Fading = false;

	private static FadeTransition Instance
	{
		get
		{
			if (m_Instance == null)
			{
				m_Instance = (new GameObject ("FadeTransition")).AddComponent<FadeTransition>();
			}
			return m_Instance;
		}
	}

	// Call when Fading is initiated
	public static bool Fading
	{
		get
		{
			return Instance.m_Fading;
		}
	}

	private void Awake()
	{
		DontDestroyOnLoad (this);
		m_Instance = this;
		m_Material = new Material ("Shader \"Plane/No zTest\" { SubShader { Pass { Blend SrcAlpha OneMinusSrcAlpha ZWrite Off Cull Off Fog { Mode Off } BindChannels { Bind \"Color\",color } } } }");
	}

	// Draws a quad to allow the items to fade
	private void DrawQuad(Color aColor,float aAlpha)
	{
		aColor.a = aAlpha;
		m_Material.SetPass (0);
		GL.Color (aColor);

		GL.PushMatrix ();
		GL.LoadOrtho ();
		GL.Begin (GL.QUADS);
		GL.Vertex3 (0, 0, -1);
		GL.Vertex3 (0, 1, -1);
		GL.Vertex3 (1, 1, -1);
		GL.Vertex3 (1, 0, -1);
		GL.End ();
		GL.PopMatrix ();
	}

	// Fading sequence alpha to solid, LOAD LEVEL NAME , solid to alpha then turn off fading
	private IEnumerator Fade (float aFadeOutTime, float aFadeInTime, Color aColor)
	{
		float time = 0.0f;

		// Fade out old scene. Alpha to Solid
		while (time < 1.0f)
		{
			yield return new WaitForEndOfFrame();
			time = Mathf.Clamp01(time + Time.deltaTime / aFadeOutTime);
			DrawQuad (aColor,time);
		}

		//Load level by name
		if (m_LevelName != "")
		{
			Application.LoadLevel (m_LevelName);
		}

		// Fade in to loaded scene. Solid to Alpha
		while (time > 0.0f)
		{
			yield return new WaitForEndOfFrame();
			time = Mathf.Clamp01(time - Time.deltaTime / aFadeInTime);
			DrawQuad(aColor,time);
		}
		m_Fading = false;
	}

	// Start fade
	private void StartFade(float aFadeOutTime,float aFadeInTime, Color aColor)
	{
		m_Fading = true;
		StartCoroutine (Fade (aFadeOutTime, aFadeInTime, aColor));
	}

	// Load level with fading effect. Instead of Application.LoadLevel can use AutoFade.LoadLevel
	public static void LoadLevel (string aLevelName,float aFadeOutTime, float aFadeInTime, Color aColor)
	{
		if (Fading) return;
		Instance.m_LevelName = aLevelName;

		// When calling new scene. Use Autofade.LoadLevel("LevelName", InTime,OutTime, Color of fade)
		Instance.StartFade (aFadeOutTime, aFadeInTime, aColor);
	}
}
