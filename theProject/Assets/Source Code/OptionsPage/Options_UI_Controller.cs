﻿using UnityEngine;
using System.Collections;

public class Options_UI_Controller : MonoBehaviour 
{
	public Vector2 startTouch = Vector2.zero;

	public float OPT_OPEN = 0.0f;
	public float OPT_CLOSE = 0.0f;

	// Use this for initialization
	void Start () 
	{
	}

	IEnumerator DelayStart()
	{
		GameObject open = GameObject.Find ("options_menu");
		yield return new WaitForSeconds (1.0f);
		open.transform.Translate (new Vector2 ((OPT_OPEN - open.transform.position.x) * 0.05f , 0.0f));

	}

	IEnumerator DelayEnd()
	{
		GameObject close = GameObject.Find ("options_menu");
		close.transform.Translate (new Vector2 ((OPT_CLOSE - close.transform.position.x) * -0.05f, 0.0f));
		yield return new WaitForSeconds (1.0f);
	}

	// Update is called once per frame
	void Update () 
	{
		StartCoroutine (DelayStart ());

		foreach (Touch touch in Input.touches) 
		{
			startTouch = touch.position;
			Ray ray = Camera.main.ScreenPointToRay(touch.position);
			RaycastHit hit;

			if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began) 
			{
				if(Physics.Raycast(ray, out hit))
				{
					// back Button Interface
					GameObject back = GameObject.Find ("back");
					if(hit.collider.gameObject == back)
					{
						//closes the options file and delays the fade by 1 Sec
						//StartCoroutine (DelayEnd());

						//Quits the Application
						FadeTransition.LoadLevel("StartMenuScene",2,1,Color.black);
					}
					
				}
			}
		}
	}
}
