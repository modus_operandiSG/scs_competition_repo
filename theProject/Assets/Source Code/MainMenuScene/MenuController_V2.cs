﻿using UnityEngine;
using System.Collections;

public class MenuController_V2 : MonoBehaviour 
{
	public Vector2 startTouch = Vector2.zero;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		foreach (Touch touch in Input.touches) 
		{
			startTouch = touch.position;
			Ray ray = Camera.main.ScreenPointToRay(touch.position);
			RaycastHit hit;

			if(Physics.Raycast(ray, out hit))
			{
				if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began)
				{
					GameObject sound = GameObject.Find ("AudioManager");
					SoundManager buttonSound = sound.GetComponent <SoundManager> ();

					//Community button
					GameObject cmty = GameObject.Find ("CommunityUP");
					if(hit.collider.gameObject == cmty)
					{
						buttonSound.MenuButtons();

						//Load Level
						FadeTransition.LoadLevel ("ShiokScene",0.5f,0.5f,Color.black);
						cmty.renderer.enabled = true;
					}

					//SG50 Button
					GameObject sgFifty = GameObject.Find("SG50");
					if(hit.collider.gameObject == sgFifty)
					{
						buttonSound.MenuButtons();

						//Opens SG50 URL
						Application.OpenURL("http://www.singapore50.sg/");
					}

					//Tutorial Button
					GameObject tutBtn = GameObject.Find("TutorialButton");
					if(hit.collider.gameObject == tutBtn)
					{
						buttonSound.MenuButtons();

						//Tutorial Button
						tutBtn.renderer.enabled = true;
					}
					// Credits Button
					GameObject credsBtn = GameObject.Find("CreditsButton");
					if(hit.collider.gameObject == credsBtn)
					{
						buttonSound.MenuButtons();

						FadeTransition.LoadLevel ("CreditsScene",0.5f,0.5f,Color.black);

						//Credits Button
						credsBtn.renderer.enabled = true;
					}
				}
			}
		}
	}
}
