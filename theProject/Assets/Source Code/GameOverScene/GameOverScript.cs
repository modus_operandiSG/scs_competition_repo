﻿using UnityEngine;
using System.Collections;

public class GameOverScript : MonoBehaviour 
{
	public Vector2 startTouch = Vector2.zero;


	//font manipulation
	public GUISkin guiSkin; 
	GUIStyle largeScore;
	GUIStyle largeMoney;

	// Use this for initialization
	void Start () 
	{
		largeScore = new GUIStyle ();
		largeMoney = new GUIStyle ();
		largeScore.fontSize = 53;
		largeMoney.fontSize = 53;
		largeScore.normal.textColor = Color.blue;
		largeMoney.normal.textColor = Color.green;
	}
	
	// Update is called once per frame
	void Update () 
	{
		foreach (Touch touch in Input.touches) 
		{
			startTouch = touch.position;
			Ray ray = Camera.main.ScreenPointToRay(touch.position);
			RaycastHit hit;
			
			if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began) 
			{
				GameObject pointsCarry = GameObject.Find ("PointsCarrying");
				GameObject gameMusic = GameObject.Find ("GameMusic");
				if(Physics.Raycast(ray, out hit))
				{
					//Load Level
					GameObject RestartButton = GameObject.Find ("Restart");
					if(hit.collider.gameObject == RestartButton)
					{
						// Destroy the prefab object so that it does not carry over
						Destroy(pointsCarry);
						Destroy (gameMusic);
						//Load Level
						FadeTransition.LoadLevel ("ShiokScene",2,1,Color.black);
					}

					//Load Level
					GameObject StartMenu = GameObject.Find ("StartMenuButton");
					if(hit.collider.gameObject == StartMenu)
					{
						// Destroy the prefab object so that it does not carry over
						Destroy(pointsCarry);
						//Load Level
						FadeTransition.LoadLevel ("StartMenuScene",2,1,Color.black);
					}
				}
			}
		}
	}

	void OnGUI()
	{
		GUI.skin = guiSkin;

		GameObject go = GameObject.Find ("PointsCarrying");
		PointsCarry pp = go.GetComponent <PointsCarry> ();

		// Points
		GUI.Label (new Rect ((Screen.width * 351) / 1920, (Screen.height * 233) / 1080, (Screen.width * 120) / 1920, (Screen.height * 50) / 1080), "" + pp.points,largeScore);
		// Money
		GUI.Label (new Rect ((Screen.width * 1210) / 1920, (Screen.height * 233) / 1080, (Screen.width * 120) / 1920, (Screen.height * 50) / 1080), "" + pp.money,largeMoney);
	}
}
